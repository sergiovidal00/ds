package e1.VendingMachine;

import e1.EuroCoin.EuroCoin;
import java.util.List;
import java.util.Collections;

public class CambioSimple implements Cambio{
    
    @Override
    public List cambio(int precio, List <EuroCoin> monedas){
        EuroCoin currentcoin;
        Collections.sort(monedas);
        while(precio>0){
            currentcoin=monedas.get(0);
            precio-=currentcoin.getValor().getValor();
            monedas.remove(0);
        }
        return monedas; 
    }    
}
