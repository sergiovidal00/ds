package e1.VendingMachine;

import e1.EuroCoin.EuroCoin;
import java.util.*;

public class VendingMachine {
    List <Producto> productlist = new ArrayList<>();
    List <EuroCoin> coins = new ArrayList<>();
    Cambio cambio = new CambioSimple();
    
   
   void setCambio (Cambio cambio){
       this.cambio=cambio;
   } 
   void insertProduct (String product, int price ){
       Producto producto = new Producto(product, price);
       if (productlist.contains(producto)) {}
       else productlist.add(producto); 
   }
   
   void insertCoin (EuroCoin e){
      coins.add(e); 
   }
   
   List <EuroCoin> buy (String product){
       return coins;
   }
   
   List<EuroCoin> cancel () {
       List <EuroCoin> devolver = new ArrayList<>();
       devolver.addAll(coins);
       coins.removeAll(devolver);
       return devolver;
   }
   
   
}
