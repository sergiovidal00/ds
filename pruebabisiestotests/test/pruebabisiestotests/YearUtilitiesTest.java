/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebabisiestotests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sergio
 */
public class YearUtilitiesTest {
    
    public YearUtilitiesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod() {
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of isLeap method, of class YearUtilities.
     */
    @Test
    public void testIsLeap() {
        System.out.println("isLeap");
        int year = 4;
        boolean expResult = true;
        boolean result = YearUtilities.isLeap(year);
        assertEquals(expResult, result);  
        
        assertTrue(YearUtilities.isLeap(8));
        assertFalse(YearUtilities.isLeap(5));
        assertFalse(YearUtilities.isLeap(100));
        assertFalse(YearUtilities.isLeap(200));
        assertTrue(YearUtilities.isLeap(400)); 
        
    }
    
}
