package e4;

import java.util.Objects;

enum Valor {
        e2(200),
        e1(100),
        c50(50),
        c20(20),
        c10(10),
        c5(5),
        c2(2),
        c1(1);
        
        private final int value;
        
        public int GetValor() {
            return value;
        }
        
        Valor (int value){
            this.value=value;
        }        
        
        
    }
    
    enum Pais {AT,BE,CY, NL, EE, FI, FR, DE, GR, IE, IT, LV, LT, LU, MT, MC, PT, SM, SK, SI, ES, VA};
    
    enum Color {Oro, Bronce, Oro_Plata};

public class EuroCoin {
        
    private final Valor valor;
    
    private final Pais pais;
    
    private final Color color;
    
    private final String diseño;
    
    private final int year;

    public EuroCoin (Valor value, Pais country, Color colour, String design, int date  ){
        valor=value;
        pais= country;
        color=colour;
        diseño=design;
        year=date;
    }

    public Valor getValor() {
        return valor;
    }

    public Pais getPais() {
        return pais;
    }

    public Color getColor() {
        return color;
    }

    public String getDiseño() {
        return diseño;
    }
    
    public int getYear() {
        return year;
    }
    
    @Override
public boolean equals ( Object obj ) {
    
  if (this==obj) {return true;}
  
  if (obj==null) {return false;}
  
  if (getClass()!=obj.getClass()) { return false;}
  
  EuroCoin other = (EuroCoin) obj;
  
  return (((this.valor==other.valor)&&(this.pais==other.pais))&&((this.color==other.color)&&(this.diseño.equals(other.diseño))));
}

    @Override
public int hashCode() {
    int hash = 7;
    hash = 37 * hash + Objects.hashCode(this.valor);
    hash = 37 * hash + Objects.hashCode(this.pais);
    hash = 37 * hash + Objects.hashCode(this.color);
    hash = 37 * hash + Objects.hashCode(this.diseño);
    return hash;
    }


        
}




