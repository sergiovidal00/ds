package e4;

import java.util.*;

// Represents a Euro coin collection
public class EuroCoinCollection {
    List <EuroCoin> collection = new ArrayList();
        
// Inserts a coin in the collection . If the coin is already in the
// collection ( there is an equal coin inserted ) then the coin is not inserted .
// Returns true only if a new coin has been inserted in the collection .
public boolean insertCoin ( EuroCoin coin ) {
    boolean decision;
    
if (collection.contains(coin)) {decision=false;}
else {collection.add(coin);
  decision=true;      
}

return decision;
}
// Checks if a coin has been already inserted in the collection
public boolean hasCoin ( EuroCoin coin ) { 
        return collection.contains(coin);
}
// Returns the nominal value of the entire collection in euro cents .
public int value () { 
    EuroCoin currentcoin;
    int totalvalue = 0;
for (int i=0; i<collection.size(); i++){
    currentcoin= collection.get(i);
    totalvalue += currentcoin.getValor().GetValor();
    }
return totalvalue;
}
// Counts the number of coins in the collection .
public int numCoins () { 
    return collection.size();
}
// Removes the specified coin from the collection
public void removeCoin ( EuroCoin coin ) {
collection.remove(coin);


}
}