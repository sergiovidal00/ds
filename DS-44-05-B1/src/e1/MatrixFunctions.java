package e1;

public class MatrixFunctions {
    
    
    // Returns the maximun value of a matrix
    public static int max ( int [][] a) { 
        int max;
        max=Integer.MIN_VALUE;
        for (int i=0; i<a.length; i++)
         for (int j=0; j<a[i].length;j++)
             if (max<a[i][j]) max=a[i][j];
           
        return max;  
    } 
    
    // Returns the sum of the values of a given row
    public static int rowSum ( int [][] a , int row ) { 
     int sum;
     sum=0;
     for (int j=0;j<a[row].length; j++)
         sum=sum+a[row][j];
     return sum;
    }
    
    // Returns the sum of the values of a given column
    public static int columnSum ( int [][] a , int column ) {
    int sum;
     sum=0;
     for (int i=0;i<a.length; i++)
         if (column>(a[i].length-1)) {}
         else sum=sum+a[i][column];
     return sum;}
    
// Sums the value of each row and returns the results in an array .
    public static int [] allRowSums ( int [][] a) { 
     int[] result;
     int sum;
     
     result = new int [a.length];
     sum=0;
     
     for (int i=0; i<a.length; i++){
         
         for (int j=0; j<a[i].length;j++)
             sum=sum+a[i][j];
         
         result[i]=sum;
         sum=0;
     }
     return result;
    
    }
    
// Sums the value of each column and returns the results in an array .
// If a position does not exist because the array is " ragged " that position
// is considered a zero value .
    public static int [] allColumnSums ( int [][] a) { 
    int[] result;
    int maxlength;
    
    maxlength=0;
    
    for (int i=0; i<a.length; i++)
        if (maxlength<a[i].length) maxlength=a[i].length;
    
    result= new int [maxlength];
    
    for(int i=0; i<result.length;i++)
        result[i]=0;
    
    for (int i=0; i<a.length; i++)
        for (int j=0; j<a[i].length; j++)
            result[j]=result[j]+ a[i][j];
    
    return result;
    
    }
    
    
// Checks if an array is "row - magic ", that is , if all its rows have the same
// sum of all its values .
    public static boolean isRowMagic ( int [][] a) { 
      int [] resultado;
      boolean magic;
      resultado = MatrixFunctions.allRowSums(a);
      magic=true;
      
      for (int i=0;i<resultado.length;i++)
          if (resultado[0]!=resultado[i]) magic=false;
      
      return magic;
    
    }
    
// Checks if an array is " column - magic ", that is , if all its columns have
// the same sum of all its values .
    public static boolean isColumnMagic ( int [][] a) { 
    int [] resultado;
      boolean magic;
      resultado = MatrixFunctions.allColumnSums(a);
      magic=true;
      
      for (int i=0;i<resultado.length;i++)
          if (resultado[0]!=resultado[i]) magic=false;
      
      return magic;
    }
            
            
// Checks that a matrix is square , that is , it has the same number of rows
// as columns and all rows have the same length .
    public static boolean isSquare ( int [][] a) { 
    boolean square;
    square= true;
    
    for (int i=0; i<a.length; i++)
        if (a.length!=a[i].length) square=false;
    
    return square;
    
    }
    
    
// Check if the matrix is a magic square . A matrix is magic square if it is
// square , all the rows add up to the same , all the columns add up to the
// same and the two main diagonals add up to the same . Also all these sums
// are the same .
    public static boolean isMagic ( int [][] a ) {
    boolean magic;
    int leftdiag;
    int rightdiag;
    int [] rowsum;
    int [] columnsum;
    
    magic=MatrixFunctions.isColumnMagic(a)&& MatrixFunctions.isRowMagic(a) && MatrixFunctions.isSquare(a);
    
    if (magic) {
        leftdiag=0;
        rightdiag=0;
        for (int i=0; i<a.length; i++)
            for (int j=0; j<a[i].length;j++){
                if (i==j) leftdiag=leftdiag + a[i][j];
                if (i+j==(a.length-1)) rightdiag= rightdiag + a[i][j];
            }
        rowsum=MatrixFunctions.allRowSums(a);
        columnsum=MatrixFunctions.allColumnSums(a);
        
        if (leftdiag!=rightdiag) magic=false;
        
        if ((leftdiag==columnsum[0])&&(rightdiag==rowsum[0])) {}
        else{
                magic=false;
                }
    }
return magic;       
    }
    
    
    
    
// Checks if the given matrix forms a sequence , that is , it is square
// (of order n) and contains all the digits from 1 to n * n, regardless of
// their order .
    public static boolean isSequence ( int [][] a) { 
    boolean sequence;
    int [] number;
    int newlength;
    int temp;
    
    sequence=MatrixFunctions.isSquare(a);
    
    if (sequence) {
        newlength=a.length*a.length;
        number= new int [newlength];
        
        for (int i=0; i<newlength;i++)
            number[i]=i+1;
        
        for (int i=0; i<a.length;i++)
            for( int j=0; j<a[i].length;j++){
                int k=0;
                while (k<newlength && a[i][j]!=number[k]) k=k+1;
                if (k==newlength) {sequence=false;}
                else {temp=number[k]; number[k]=number[newlength-1]; number[newlength-1]=temp;
                newlength--;
                }
            }
                
    }
    return sequence;
                
                
                
                
                
            }
            
    }
    

