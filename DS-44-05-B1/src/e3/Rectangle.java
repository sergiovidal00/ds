package e3;

// Represents a rectangle
public class Rectangle {
    private int x, y;
// Initializes a new rectangle with the values passed by parameter .
// Throws IllegalArgumentException if a a negative value is passed to any of
// the dimensions .
public Rectangle ( int base , int height ) { 
  if ((base<0) || (height<0)) {throw new IllegalArgumentException();}
  else {x=base;
        y=height;}
  }

// Copy constructor
public Rectangle ( Rectangle r ) { 
x=r.x;
y=r.y;
}

// Getters
public int getBase () { 
return x;
}
public int getHeight () { 
return y;
}
// Setters . Throw IllegalArgumentException if the parameters are negative .
public void setBase ( int base ) {
    if (base<0) throw new IllegalArgumentException();
    else x=base;
}
public void setHeight ( int height ) {
    if (height<0) throw new IllegalArgumentException();
    else y=height;
}
// Return true if the rectangle is a square
public boolean isSquare () { 
        return x==y;
}
// Calculate the area of the rectangle .
public int area () {
    return x*y;
}
// Calculate the perimeter of the rectangle .
public int perimeter () { 
    return (2*x + 2*y);
}
// Calculate the length of the diagonal
public double diagonal () {
    return Math.pow((Math.pow(x, 2.) + Math.pow(y, 2.)),0.5);
}
// Turn this rectangle 90 degrees ( changing base by height ).
public void turn () { 
int temp;
temp=x; x=y; y=temp;
}
// Ensure that this rectangle is oriented horizontally ( the base is greater
// or equal than the height ).
public void putHorizontal () {
if (x>y) {}
else {this.turn();}
}
// Ensure that this rectangle is oriented vertically ( the height is greater
// or equal than the base ).
public void putVertical () {
if (x>y) {this.turn();}
}
// Two rectangles are equal if the base and the height are the same .
// It does not take into account if the rectangle is rotated .
@Override
public boolean equals ( Object obj ) {
    
  if (this==obj) {return true;}
  
  if (obj==null) {return false;}
  
  if (getClass()!=obj.getClass()) { return false;}
  
  Rectangle other = (Rectangle) obj;
  
  return (((this.x==other.x)&&(this.y==other.y))||((this.x==other.y)&&(this.y==other.x)));
}
// It complies with the hashCode contract and is consistent with the equals .
    @Override 
    public int hashCode () { 
    int hash = 3;
    hash = 79 * hash + this.x*this.y;
    return hash;
    }
}
