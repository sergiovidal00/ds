
package clase1teoria;

public class Caja {
    private int valor;
    
    public int GetValor() {
        return valor;
    }
    
    public void setValor (int x) {
        valor= x;
    }

    public Caja(int x){
        valor=x;
    }
    
    public boolean dequals (Caja x){
        boolean cosa;
        if (x.valor==valor) 
            cosa=true;
        else
            cosa=false;
        
        return cosa;
        
        
                   
    }
    
    @Override
    public int hashCode() {
        int hash=3;
        hash = 59* hash +this.valor;
        return hash;
    }
    
    @Override 
    public boolean equals (Object x){
        if (this ==x) {
            return true;
        }
        if (x==null ) {
            return false;
        }

        if (this.getClass() != x.getClass()){
            return false;
        }
        
        //Son de la misma clase y obj no es nulo 
        Caja other = (Caja) x;
        if (this.valor != other.valor) {
            return false;
        }
        return true;
}
   
}
