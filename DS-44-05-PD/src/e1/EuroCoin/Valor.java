package e1.EuroCoin;

public enum Valor {
        E2(200,Color.Oro_Plata),
        E1(100,Color.Oro_Plata),
        C50(50, Color.Oro),
        C20(20, Color.Oro),
        C10(10,Color.Oro),
        C5(5,Color.Bronce),
        C2(2,Color.Bronce),
        C1(1,Color.Bronce);
        
        private final int value;
        private final Color color;
        
        public int getValor() {
            return value;
        }
        
        public Color getColor() {
            return color;
        }
        Valor (int value, Color color){
            this.value=value;
            this.color=color;
        }  
        
        
        
}
