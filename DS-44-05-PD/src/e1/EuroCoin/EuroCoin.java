package e1.EuroCoin;

import java.util.Objects;



public final class EuroCoin implements Comparable<EuroCoin> {
    
    
        
    private final Valor valor;
    
    private final Pais pais;
    
    
    private final String diseño;
    
    private final int year;

    public EuroCoin (Valor value, Pais country, String design, int date  ){
        valor=value;
        pais= country;
        diseño=design;
        year=date;
    }

    public Valor getValor() {
        return valor;
    }

    public Pais getPais() {
        return pais;
    }

    public Color getColor() {
        return valor.getColor();
    }

    public String getDiseño() {
        return diseño;
    }
    
    public int getYear() {
        return year;
    }
    
    @Override
public boolean equals ( Object obj ) {
    
  if (this==obj) {return true;}
  
  if (obj==null) {return false;}
  
  if (getClass()!=obj.getClass()) { return false;}
  
  EuroCoin other = (EuroCoin) obj;
  
  return (((this.valor==other.valor)&&(this.pais==other.pais))&&(this.diseño.equals(other.diseño)));
}

    @Override
public int hashCode() {
    int hash = 7;
    hash = 37 * hash + Objects.hashCode(this.valor);
    hash = 37 * hash + Objects.hashCode(this.pais);
    hash = 37 * hash + Objects.hashCode(this.diseño);
    return hash;
    }

    @Override
public int compareTo (EuroCoin moneda){
    if (this.valor==moneda.valor){
        if (this.pais==moneda.pais){
            return (this.diseño.compareToIgnoreCase(moneda.diseño));
        }
        else return (this.pais.compareTo(moneda.pais));
    }
    else return (this.valor.compareTo(moneda.valor));
                  
}



        
}




