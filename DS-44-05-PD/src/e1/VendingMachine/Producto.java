package e1.VendingMachine;

import java.util.Objects;

public final class Producto {
    private final String name;
    private final int price;

    public Producto(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }
    
    
    @Override
    public boolean equals ( Object obj ) {
    
        if (this==obj) {return true;}

        if (obj==null) {return false;}

        if (getClass()!=obj.getClass()) { return false;}

        Producto other = (Producto) obj;

        return ((this.name.equals(other.name)));
  }

      @Override
    public int hashCode() {
      int hash = 7;
      hash = 37 * hash + Objects.hashCode(this.name);
      return hash;
      }
}
