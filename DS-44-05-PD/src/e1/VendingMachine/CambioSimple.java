package e1.VendingMachine;

import e1.EuroCoin.EuroCoin;
import java.util.List;
import java.util.Collections;

public class CambioSimple implements Cambio{
    
    /*En el sistema de cambio simple en prime rlugar se ordenará la lista para que primero gaste las monedas de mas alto valor y despues las de menor valor,
    despues comenzará a obtener monedas de la lista, hasta que el precio sea menor o igual que 0 o las monedas se acaben, en tal caso devolveremos una
    IllegalArgumentException. 
    */    
    
    @Override
    public List cambio(int precio, List <EuroCoin> monedas){
        EuroCoin currentcoin;
        Collections.sort(monedas);
        while(precio>0){
            try {currentcoin = monedas.get(0);}
            catch (Exception e){
                throw new IllegalArgumentException();
            }
            precio-=currentcoin.getValor().getValor();
            monedas.remove(0);
        }
        return monedas; 
    }    
}
