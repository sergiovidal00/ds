package e1.VendingMachine;

import e1.EuroCoin.EuroCoin;
import java.util.*;

public class VendingMachine {
    private List <Producto> productlist = new ArrayList<>();
    private List <EuroCoin> coins = new ArrayList<>();
    private Cambio cambio = new CambioSimple();
    
   
   void setCambio (Cambio cambio){
       this.cambio=cambio;
   }
   
   void insertProduct (String product, int price ){
       Producto producto = new Producto(product, price);
       if (productlist.contains(producto)) {}
       else productlist.add(producto); 
   }
   
   void insertCoin (EuroCoin e){
      coins.add(e); 
   }
  
   /*Cuando se ejectute buy() se comprobará que el producto está en la lista de productos, si está se pedirá cuanto cambio debe devolverse dependiendo
   del atributo cambio y si no se encuentra se lanzara una IllegalArgumentException
   */
   
   List <EuroCoin> buy (String product){
       int precio=0;
       List <EuroCoin> vuelta;
       
       for (Producto pro : productlist){
          if (pro.getName().equals(product)) precio=pro.getPrice(); 
       }
       
       if (precio==0) throw new IllegalArgumentException();
       
       vuelta = cambio.cambio(precio, coins);
       return vuelta;
   }
   /*Para cancelar la operación, haremos una nueva lista que copiara todos los elementos de las monedas introducidas, para luego eliminarlas de dicha lista
   y devolver la nueva lista creada.
   */
   List<EuroCoin> cancel () {
       List <EuroCoin> devolver = new ArrayList<>();
       devolver.addAll(coins);
       coins.removeAll(devolver);
       return devolver;
   }
   
   
}
