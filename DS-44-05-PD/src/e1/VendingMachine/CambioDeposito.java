package e1.VendingMachine;

import e1.EuroCoin.*;
import java.util.List;
import java.util.Collections;
import java.util.ArrayList;

public class CambioDeposito implements Cambio{
    
    /*En la función MaxCambio realizaremos un bucle buscando el valor mayor que podamos darle a la moneda que devolveremos, tras esto se creará
    dicha moneda y se devolverá.
    */
    
    private EuroCoin MaxCambio (int cantidad){
      int i=0;
      boolean found = false;
      Valor value = null; 
      Valor valor []= new Valor[]{Valor.E2,Valor.E1,Valor.C50,Valor.C20,Valor.C10,Valor.C5,Valor.C2,Valor.C1};
      
      while (i<=valor.length && found==false){
           if (valor[i].getValor()<=cantidad){
               found=true;
               value = valor[i];
           }
           else i++; 
       }
        
      return new EuroCoin(value, Pais.ES, "Juan Carlos 1", 2000);
    }
    
    /*En el cambio deposito el procedimiento es similar al del cambio simple pues iremos sacando monedas de la lista hasta que nos quedemos con el precio
    igual o menor que 0, en tal caso comprobaremos cuanto sobró del valor de la ultima moneda introducida y mediante la funciónMaxCambio iremos devolviendo
    monedas hasta que dicha cantidad sea 0. Tras esto añadiremos esa lista a la de monedas sobrantes del pago y la devolveremos.
    */
    
    @Override
    public List cambio(int precio, List <EuroCoin> monedas){
        EuroCoin currentcoin;
        int restante;
        EuroCoin moneda;
        List <EuroCoin> cambios = new ArrayList <> ();
        Collections.sort(monedas);
        while (precio>0){
            try {currentcoin = monedas.get(0);}
            catch (Exception e){
                throw new IllegalArgumentException();
            }
            monedas.remove(0);
            restante = currentcoin.getValor().getValor() - precio;
            precio-=currentcoin.getValor().getValor();
            if (precio<=0){
                while (restante!=0) {
                    moneda=MaxCambio(restante);
                    cambios.add(moneda);
                    restante-=moneda.getValor().getValor();
                } 
            }
        }
        cambios.addAll(monedas);
        Collections.sort(cambios);
        return cambios;
    }
}
