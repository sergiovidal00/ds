package e2;

import java.util.*;

public class ClienteSimple implements Observer{
    
    private Map <String, Double> ActionList = new HashMap<>();
    
    /*En caso de que se pida el valor de cierre de una acción se debe suministrar el nombre de dicha acción, que se comprobará si es la key de alguno
    de los valores del mapa y si lo es, se devolverá el valor asociado a ella. En caso de que no sea key de ninguno se lanzara una IllegalArgumentException
    */
    
    public double getCierre(String nombreaccion) {
        if (ActionList.containsKey(nombreaccion)){
            return ActionList.get(nombreaccion);
        }
        else throw new IllegalArgumentException();
    }
    
    
    /*Cuando se realice un notifyObservers el update comprobará si el nombre de la acción es key de alguno de los valores del mapa, si lo es, actualizará el
    valor de cierre, si no lo es, se añadira al mapa junto a su valor de cierre
    */
    
    
    @Override
    public void update(Observable o, Object arg){
        Accion accion  = (Accion) o;
        
        if (ActionList.containsKey(accion.getSimbolo())){
            ActionList.replace(accion.getSimbolo(), accion.getCierre());
        }
        else {
            ActionList.put(accion.getSimbolo(), accion.getCierre());
        }
    }    
}
