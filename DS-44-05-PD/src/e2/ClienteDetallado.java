package e2;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class ClienteDetallado implements Observer{
    
    private List <Accion> ActionList = new ArrayList<>(); 
    
    
    
    /*Para que el Cliente devuelva los datos que le pidamos debemos proporcionar el nombre de la accion sobre la que los pedimos,
    los getters comprobarán si dicha acción se encuentra en su lista y devolverá lo que le pedimos si la encuentra. En caso de no 
    encontrarla lanzará una IllegalArgumentException() 
    */
    
    public double getCierre(String nombreaccion) {
        int i=0;
        boolean found=false;
        while (i<ActionList.size() && found==false){
            if (ActionList.get(i).getSimbolo().equals(nombreaccion)) 
                found=true;
            else i++;
        }
        if (found==true) 
            return ActionList.get(i).getCierre();
        else throw new IllegalArgumentException();
    }

    public double getMaximo(String nombreaccion) {
        int i=0;
        boolean found=false;
        while (i<ActionList.size() && found==false){
            if (ActionList.get(i).getSimbolo().equals(nombreaccion)) 
                found=true;
            else i++;
        }
        if (found==true) 
            return ActionList.get(i).getMaximo();
        else throw new IllegalArgumentException();
    }
    
    public double getMinimo(String nombreaccion) {
        int i=0;
        boolean found=false;
        while (i<ActionList.size() && found==false){
            if (ActionList.get(i).getSimbolo().equals(nombreaccion)) 
                found=true;
            else i++;
        }
        if (found==true) 
            return ActionList.get(i).getMinimo();
        else throw new IllegalArgumentException();
    }

    public int getVolumen(String nombreaccion) {
        int i=0;
        boolean found=false;
        while (i<ActionList.size() && found==false){
            if (ActionList.get(i).getSimbolo().equals(nombreaccion)) 
                found=true;
            else i++;
        }
        if (found==true) 
            return ActionList.get(i).getVolumen();
        else throw new IllegalArgumentException();
    }
    
    
    
    /*Al realizar un notifyObservers el update comprobará si la acción sobre la que se realizó está en la lista, si está, se actualizan los atributos
    de dicho elemento en la lista, si no está se añade una copia de dicha accion a la lista.
    */
    
    @Override
    public void update(Observable o, Object arg){
        int i;
        Accion action = (Accion) o;
        
        if (ActionList.contains(action)){
            i=ActionList.indexOf(action); 
            ActionList.get(i).setCierre(action.getCierre());
            ActionList.get(i).setMaximo(action.getMaximo());
            ActionList.get(i).setMinimo(action.getMinimo());
            ActionList.get(i).setVolumen(action.getVolumen()); 
        }
        else {
            Accion nueva = new Accion (action.getSimbolo(), action.getCierre(), action.getMaximo(), action.getMinimo(), action.getVolumen());
            ActionList.add(nueva);
        }
        
    }    

}
    
    