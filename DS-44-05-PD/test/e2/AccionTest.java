/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e2;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sergio
 */
public class AccionTest {
    Accion AAPL,MSFT, COLG;
    ClienteSimple simple1;
    ClienteDetallado detallado1, detallado2;
    
    @Before
    public void SetUp() {
        AAPL = new Accion("AAPL", 2.2, 3.5, 1, 20);
        MSFT = new Accion("MSFT", 2, 5, 2, 30);
        COLG = new Accion ("COLG", 4, 7,2, 35);
        
        simple1= new ClienteSimple();
        detallado1 = new ClienteDetallado();
        
        AAPL.addObserver(simple1);
        MSFT.addObserver(simple1);
        AAPL.addObserver(detallado1);
        MSFT.addObserver(detallado1);
    }
    
    @Test
    public void testObserverSimple() {
        AAPL.setCierre(3.2);
        MSFT.setCierre(3.2);
        assertTrue(3.2==simple1.getCierre("AAPL"));
        assertTrue(3.2==simple1.getCierre("MSFT"));
    }
    
    @Test
    public void testObserverDetallado1(){
        AAPL.setCierre(3.2);
        AAPL.setMaximo(3.2);
        AAPL.setMinimo(1.4);
        AAPL.setVolumen(52);
        MSFT.setCierre(3.5);
        MSFT.setMaximo(3.5);
        MSFT.setMinimo(2.1);
        MSFT.setVolumen(88);
        
        assertTrue(3.2==detallado1.getCierre("AAPL"));
        assertTrue(3.2==detallado1.getMaximo("AAPL"));
        assertTrue(1.4==detallado1.getMinimo("AAPL"));
        assertTrue(52==detallado1.getVolumen("AAPL"));
        assertTrue(3.5==detallado1.getCierre("MSFT"));
        assertTrue(3.5==detallado1.getMaximo("MSFT"));
        assertTrue(2.1==detallado1.getMinimo("MSFT"));
        assertTrue(88==detallado1.getVolumen("MSFT"));
        
    }
    
    @Test (expected = IllegalArgumentException.class)
    public void testExceptionAccion() {
        Accion accion2 = new Accion("Accionisimo", 0.2, 0.2, 0.2, 20);
    }
    
    @Test (expected = IllegalArgumentException.class)
    public void testExceptionSimple() {
        simple1.getCierre("COLG");
    }
    
    
    @Test (expected = IllegalArgumentException.class)
    public void testExceptionDetallado1() {
        detallado1.getCierre("COLG");
    }
    
    
    @Test (expected = IllegalArgumentException.class)
    public void testExceptionDetallado2() {
        detallado1.getMaximo("COLG");
    }
    
    @Test (expected = IllegalArgumentException.class)
    public void testExceptionDetallado3() {
        detallado1.getMinimo("COLG");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testExceptionDetallado4() {
        detallado1.getVolumen("COLG");
    }
    @Test
    public void testEquals () {
        Accion AAPL2 = new Accion("AAPL", 2.2, 3.5, 1, 20);
        Accion NoAAPL = new Accion("ABPL", 2.2, 3.5, 1, 20);
        
        assertTrue(AAPL.equals(AAPL));
        assertFalse(AAPL.equals(null));
        assertFalse(AAPL.equals("No es igual"));
        assertFalse(AAPL.equals(NoAAPL));
        assertTrue(AAPL.equals(AAPL2));
    }
    
    @Test
    public void testHashCode() {
        Accion AAPL2 = new Accion("AAPL", 2.2, 3.5, 1, 20);
        assertEquals(AAPL.hashCode(),AAPL2.hashCode());
        assertEquals(AAPL.hashCode(),AAPL.hashCode());   
    }
    
}
