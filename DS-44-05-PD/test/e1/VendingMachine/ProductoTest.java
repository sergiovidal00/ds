/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e1.VendingMachine;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sergio
 */
public class ProductoTest {
   Producto kitkat;
   
   @Before
   public void setUp() {
       kitkat = new Producto("kitkat", 90);    
   }
   
   @Test
   public void testGetters() {
       assertEquals(kitkat.getName(), "kitkat");
       assertEquals(kitkat.getPrice(), 90);
   }
   
   @Test
   public void testEquals() {
       Producto kitkat2 = new Producto("kitkat", 90);
       Producto huesitos = new Producto("huesitos", 50);
       assertTrue(kitkat.equals(kitkat));
       assertFalse(kitkat.equals(null));
       assertFalse(kitkat.equals("No es igual"));
       assertFalse(kitkat.equals(huesitos));
       assertTrue(kitkat.equals(kitkat2));
   }
   
   @Test
   public void testHashCode() {
       Producto kitkat2 = new Producto("kitkat", 90);
       assertEquals(kitkat.hashCode(),kitkat.hashCode());
       assertEquals(kitkat.hashCode(), kitkat2.hashCode()); 
   }
    
}
