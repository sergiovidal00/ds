/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e1.VendingMachine;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import e1.EuroCoin.*;
import java.util.ArrayList;
import java.util.List;


public class VendingMachineTest {
    VendingMachine machine;
    CambioSimple simple;
    CambioDeposito deposito;
    EuroCoin e2_sp1,
            e1_sp1,
            c50_sp1,
            c20_sp1,
            c10_sp1,
            c5_sp1,
            c2_sp1,
            c1_sp1;
    
    @Before
    public void setUp() {
        machine = new VendingMachine();
        simple = new CambioSimple();
        deposito = new CambioDeposito();
        e2_sp1 = new EuroCoin(Valor.E2, Pais.ES,  "Juan Carlos 1",2000);
        e1_sp1 = new EuroCoin(Valor.E1, Pais.ES,  "Juan Carlos 1",2000);
        c50_sp1 = new EuroCoin(Valor.C50, Pais.ES,  "Juan Carlos 1",2000);
        c20_sp1 = new EuroCoin(Valor.C20, Pais.ES,  "Juan Carlos 1",2000);
        c10_sp1 = new EuroCoin(Valor.C10, Pais.ES,  "Juan Carlos 1",2000);
        c5_sp1 = new EuroCoin(Valor.C5, Pais.ES,  "Juan Carlos 1",2000);
        c2_sp1 = new EuroCoin(Valor.C2, Pais.ES,  "Juan Carlos 1",2000);
        c1_sp1 = new EuroCoin(Valor.C1, Pais.ES,  "Juan Carlos 1",2000);
        
        machine.insertProduct("snickers", 90);
        machine.insertProduct("huesitos", 110);
        machine.insertProduct("manzana", 20);
        
    }
    
    @Test
    public void testBuySimple() {
        List <EuroCoin> list = new ArrayList<>();
        list.add(e1_sp1);
        machine.insertCoin(e2_sp1);
        machine.insertCoin(e1_sp1);
        
        assertEquals(list, machine.buy("huesitos"));
    }
    
    @Test (expected = IllegalArgumentException.class)
    public void testBuyException() {
        machine.insertCoin(e1_sp1);
        machine.buy("huesitos");
    }
    
    @Test (expected = IllegalArgumentException.class)
    public void testNotProduct() {
        machine.buy("conguitos");
    }
    
    @Test
    public void testCancel() {
        List <EuroCoin> list = new ArrayList<>();
        list.add(e1_sp1);
        list.add(c50_sp1);
        machine.insertCoin(e1_sp1);
        machine.insertCoin(c50_sp1);
        assertEquals(machine.cancel(), list);
    }
    
    @Test
    public void testDeposito() {
        List <EuroCoin> list;
        machine.setCambio(deposito);
        
        machine.insertCoin(e2_sp1);
        list = machine.buy("huesitos");
        assertEquals(list.get(0), c50_sp1);
        assertEquals(list.get(1), c20_sp1);
        assertEquals(list.get(2), c20_sp1);

        machine.insertCoin(e2_sp1);
        machine.insertCoin(c2_sp1);
        list = machine.buy("huesitos");
        assertEquals(list.get(0), c50_sp1);
        assertEquals(list.get(1), c20_sp1);
        assertEquals(list.get(2), c20_sp1);
        assertEquals(list.get(3), c2_sp1);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testDepositoException () {
        machine.setCambio(deposito);
        
        machine.buy("huesitos");
    }
    
    
    

    
}
