package e2_e3;

import java.util.Comparator;

public class Comparador implements Comparator<EuroCoin>{
    
    @Override
    public int compare (EuroCoin o, EuroCoin c ){
        if (o.getPais()==c.getPais()) {
            if (o.getValor()==c.getValor()) {
                return o.getYear()-c.getYear();
            }
            else return o.getValor().compareTo(c.getValor());
        }
        else return o.getPais().compareTo(c.getPais());
    }
    
}
