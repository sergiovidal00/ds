/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e2_e3;

/**
 *
 * @author Sergio
 */
public enum Pais {
        AT("Austria"),
        BE("Belgium"),
        CY("Cyprus"),
        NL("Netherlads"),
        EE("Estonia"),
        FI("Finland"),
        FR("France"),
        DE("Germany"),
        GR("Greece"),
        IE("Ireland"),
        IT("Italy"),
        LV("Latvia"),
        LT("Lithuania"),
        LU("Luxembourg"),
        MT("Malta"),
        MC("Monaco"),
        PT("Portugal"),
        SM("San Marino"),
        SK("Slovakia"),
        SI("Slovenia"),
        ES("Spain"),
        VA("Vatican City");
        
        private final String pais;

    public String getPais() {
        return pais;
    }
        Pais ( String pais){
            this.pais=pais;
        }
    }
