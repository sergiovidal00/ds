package e2_e3;

import java.util.*;

// Represents a Euro coin collection
public class EuroCoinCollection implements Iterable<EuroCoin>{ 
    List <EuroCoin> collection = new ArrayList();
    private Pais paisIterador=null;
    private int cambios=0;

    public void setPaisIterador(Pais paisIterador) {
        this.paisIterador = paisIterador;
    }
        
// Inserts a coin in the collection . If the coin is already in the
// collection ( there is an equal coin inserted ) then the coin is not inserted .
// Returns true only if a new coin has been inserted in the collection .
public boolean insertCoin ( EuroCoin coin ) {
    boolean decision;
    
if (collection.contains(coin)) {decision=false;}
else {collection.add(coin);
  decision=true;
  cambios++;
}

return decision;
}
// Checks if a coin has been already inserted in the collection
public boolean hasCoin ( EuroCoin coin ) { 
        return collection.contains(coin);
}
// Returns the nominal value of the entire collection in euro cents .
public int value () { 
    EuroCoin currentcoin;
    int totalvalue = 0;
for (int i=0; i<collection.size(); i++){
    currentcoin= collection.get(i);
    totalvalue += currentcoin.getValor().getValor();
    }
return totalvalue;
}
// Counts the number of coins in the collection .
public int numCoins () { 
    return collection.size();
}
// Removes the specified coin from the collection
public void removeCoin ( EuroCoin coin ) {
if (collection.contains (coin)) {
    collection.remove(coin);
    cambios++;
}
}

public void orderList (){
 Collections.sort(collection);
 cambios++;
}

public void orderListC (Comparator <EuroCoin> c) {
    collection.sort(c);
    cambios++;
}

@Override
public Iterator<EuroCoin> iterator(){
    return new Iterador(paisIterador);
}

private class Iterador implements Iterator <EuroCoin>{
    private int cursor=0;
    private int last=-1;
    private final Pais pais;
    private int cambiosEsperados = cambios;
    
    Iterador(Pais pais) {
        this.pais=pais;
        
    }
    
    @Override
    public boolean hasNext() {
        boolean found=false;
        int aux=cursor;
        if (cambiosEsperados!=cambios) throw new ConcurrentModificationException();
        if (this.pais==null){
            if ((cursor+1)<collection.size()) found=true;} 
        else{ 
            if (last==1) aux++;
            while ((aux < collection.size()) && (found==false)){
            if (collection.get(aux).getPais().equals(this.pais))
                found=true;    
            aux++;
            }
        }    
        return found;
    }
    
    @Override
    public EuroCoin next() {
        boolean found;
        int aux=cursor;
        found=false;
        if (cambiosEsperados!=cambios) throw new ConcurrentModificationException();
        
        if (this.pais==null){
            if (last==1) cursor++;
            if ((cursor)<collection.size()) {found=true;}}
        else{ 
            if (last==1) aux++;
            while ((aux<collection.size()) && (found==false)){
            if (collection.get(aux).getPais().equals(this.pais)){
                found=true;
                cursor=aux;}
            else aux++;
            }
        }    
      if (found==false) {
          throw new NoSuchElementException();
      }
      else {
          last=1;
          return collection.get(cursor);
      } 
    }
    
    
    @Override
    public void remove () {
        if (last==-1) throw new IllegalStateException();
        collection.remove(collection.get(cursor));
        cambios++;
        cambiosEsperados++;
        last=-1;
    }
}
@Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EuroCoinCollection other = (EuroCoinCollection) obj;
        if (this.collection.size() != other.collection.size())
        {return false;}
        
        for(int i = 0; i<this.collection.size(); i++){
           if (this.collection.get(i)!=other.collection.get(i)) {return false;}
        }
        
        return true;
                
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.collection);
        return hash;
    }
    
    



}