package e1;

public class Cliente extends Persona{
    private final int codigo;
    private int compras;
    private int porcentajedescuento;
    
    public Cliente (String nombre, String apellidos, String DNI, String direccion, int telefono, int codigo, int compras){
        super(nombre, apellidos, DNI, direccion, telefono);
        this.codigo=codigo;
        this.compras=compras;
        this.porcentajedescuento=this.compras/100;
    }
    
    public Cliente (String nombre, String apellidos, String DNI, String direccion, int codigo, int compras){
        this(nombre, apellidos, DNI, direccion, 0, codigo, compras);
    }
    
     public Cliente (String nombre, String apellidos, String DNI, int telefono, int codigo, int compras){
        this(nombre, apellidos, DNI, null, telefono, codigo, compras);
    }
    
     public Cliente (String nombre, String apellidos, String DNI, int codigo, int compras){
        this(nombre, apellidos, DNI, null, 0, codigo, compras);
    }
    

    public int getCodigo() {
        return codigo;
    }

    public int getCompras() {
        return compras;
    }

    public int getPorcentajedescuento() {
        return porcentajedescuento;
    }

    public void setCompras(int compras) {
        this.compras = compras;
        porcentajedescuento=this.compras/100;
    }
    
    public void masCompras() {
        this.compras++;
        porcentajedescuento=this.compras/100;
    }

    @Override
    
    public String toString(){
        String aux;
        aux = super.toString() + ", codigo de cliente " + Integer.toString(codigo) + ", numero de compras " + Integer.toString(compras) + " y porcentaje de descuento " + Integer.toString(porcentajedescuento) ;
        return aux;
    }
    
    @Override
    public int hashCode() {
        int hash;
        hash = super.hashCode();
        hash = 79 * hash + this.codigo;
        hash = 79 * hash + this.compras;
        hash = 79 * hash + this.porcentajedescuento;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (!super.equals(obj)) return false;
        final Cliente other = (Cliente) obj;
        if (this.codigo != other.codigo) {
            return false;
        }
        if (this.compras != other.compras) {
            return false;
        }
        return this.porcentajedescuento == other.porcentajedescuento;
    }
      
    
}
