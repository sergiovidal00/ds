package e1;

import java.util.Objects;

public abstract class Trabajador extends Persona {
    private final int numeroSS;
    private int salario;
    public enum Shift {MAÑANA, TARDE, NOCHE};
    private Shift turno;
   
    public Trabajador (String nombre, String apellidos, String DNI, String direccion, int telefono, int numeroSS, int salario, Shift turno){
        super(nombre, apellidos, DNI, direccion, telefono);
        this.numeroSS=numeroSS;
        this.salario=salario;
        this.turno=turno;
    }
    
    public Trabajador (String nombre, String apellidos, String DNI, String direccion, int numeroSS, int salario, Shift turno){
        this(nombre, apellidos, DNI, direccion,0, numeroSS, salario, turno);
    }

    public Trabajador (String nombre, String apellidos, String DNI, int telefono, int numeroSS, int salario, Shift turno){
        this(nombre, apellidos, DNI, null, telefono, numeroSS, salario, turno);
    }    
    public Trabajador (String nombre, String apellidos, String DNI, int numeroSS, int salario, Shift turno){
        this(nombre, apellidos, DNI, null, 0, numeroSS, salario, turno);
    }    

    public int getNumeroSS() {
        return numeroSS;
    }

    public int getSalario() {
        if (this.turno==Shift.NOCHE) return (salario+150);
                else return salario;
    }

    public Shift getTurno() {
        return turno;
    }

    public void setSalario(int salario) {
        this.salario = salario;
    }

    public void setTurno(Shift turno) {
        this.turno = turno;
    }
    
    @Override
    
    public String toString(){
        String aux;
        aux = super.toString() + ", cuyo numero de la seguridad social es " + Integer.toString(numeroSS) + " y salario " + Integer.toString(salario) + " y su turno es de " + turno.toString();
        return aux;
    }

    @Override
    public int hashCode() {
        int hash;
        hash = super.hashCode();
        hash = 17 * hash + this.numeroSS;
        hash = 17 * hash + this.salario;
        hash = 17 * hash + Objects.hashCode(this.turno);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        if (!super.equals(obj)) return false;
        final Trabajador other = (Trabajador) obj;
        if (this.numeroSS != other.numeroSS) {
            return false;
        }
        if (this.salario != other.salario) {
            return false;
        }
        if (this.turno != other.turno) {
            return false;
        }
        return true;
    }
    
}
