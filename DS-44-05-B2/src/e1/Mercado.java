package e1;

import java.util.*;

public class Mercado {
    private List <Cliente> clientlist = new ArrayList<>();
    private List <Trabajador> workerlist = new ArrayList<>();
    
    public boolean agregarCliente (Cliente cliente){
       if (clientlist.contains(cliente)) return false;
       else {clientlist.add(cliente);
            return true;}
    }
    
    public <T extends Trabajador> boolean agregarTrabajador (T trabajador) {
        if (workerlist.contains(trabajador)) return false;
        else {workerlist.add(trabajador);
                return true;}
    }
    
    public boolean AgregarClientes (List<Cliente> clientes){
        boolean posible = true;
        for (int i=0;i<clientes.size();i++)
            if (clientlist.contains(clientes.get(i))) posible=false;
        if (posible) {clientlist.addAll(clientes); return true;}
        else return false;
    }
    
    public boolean agregarTrabajadores (List<? extends Trabajador> trabajadores){
        boolean posible = true;
        for (int i=0;i<trabajadores.size();i++)
            if (workerlist.contains(trabajadores.get(i))) posible=false;
        if (posible) {workerlist.addAll(trabajadores); return true;}
        else return false;
    }
    
    public int SalariosMercado (){
        int contador =0;
        for (int i=0; i<workerlist.size();i++)
            contador= contador + workerlist.get(i).getSalario();
        return contador;
} 
    public List PersonasMercado (){
        List <Persona> personlist = new ArrayList<>();
        personlist.addAll(clientlist);
        personlist.addAll(workerlist);
        return personlist;
    }    
}