package e1;

import java.util.Objects;

public abstract class Persona {
    private final String nombre;
    private final String apellidos;
    private final String DNI;
    private  String direccion;
    private int telefono;
    
    
    public Persona (String nombre, String apellidos, String DNI, String direccion, int telefono){
        this.nombre=nombre;
        this.apellidos=apellidos;
        this.DNI=DNI; 
        this.direccion=direccion;
        this.telefono =telefono;
    }
    
    public Persona (String nombre, String apellidos, String DNI){
        this(nombre, apellidos, DNI, null, 0);
    }
    
    public Persona (String nombre, String apellidos, String DNI, String direccion){
        this(nombre, apellidos, DNI, direccion, 0);
    }
    
    public Persona (String nombre, String apellidos, String DNI, int telefono){
        this(nombre, apellidos, DNI, null, telefono);
    }
    
    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getDNI() {
        return DNI;
    }

    public String getDireccion() {
        return direccion;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }
    
    @Override
    public String toString(){
        String aux;
        aux = nombre + " " + apellidos + ", con DNI " + DNI + ", dirección " + direccion + ", teléfono " + Integer.toString(telefono);
        return aux;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.nombre);
        hash = 67 * hash + Objects.hashCode(this.apellidos);
        hash = 67 * hash + Objects.hashCode(this.DNI);
        hash = 67 * hash + Objects.hashCode(this.direccion);
        hash = 67 * hash + this.telefono;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Persona other = (Persona) obj;
        if (this.telefono != other.telefono) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.apellidos, other.apellidos)) {
            return false;
        }
        if (!Objects.equals(this.DNI, other.DNI)) {
            return false;
        }
        if (!Objects.equals(this.direccion, other.direccion)) {
            return false;
        }
        return true;
    }
    
    
    
}
