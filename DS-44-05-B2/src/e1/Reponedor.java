package e1;

import java.util.Objects;


public class Reponedor extends Trabajador{
    private final String empresa;
    
    public Reponedor (String nombre, String apellidos, String DNI, String direccion, int telefono, int numeroSS, int salario, Shift turno, String empresa){
        super(nombre, apellidos, DNI, direccion, telefono, numeroSS, salario, turno);
        
        if (turno==Shift.NOCHE) throw new IllegalArgumentException();
        else this.empresa=empresa;
            
    }
    
    public Reponedor (String nombre, String apellidos, String DNI, String direccion, int numeroSS, int salario, Shift turno, String empresa){
        this(nombre, apellidos, DNI, direccion, 0, numeroSS, salario, turno, empresa);
    }
    
    public Reponedor (String nombre, String apellidos, String DNI, int telefono, int numeroSS, int salario, Shift turno, String empresa){
        this(nombre, apellidos, DNI, null, telefono, numeroSS, salario, turno, empresa);
    }
    
    public Reponedor (String nombre, String apellidos, String DNI,  int numeroSS, int salario, Shift turno, String empresa){
        this(nombre, apellidos, DNI, null, 0, numeroSS, salario, turno, empresa);
    }

    public String getEmpresa() {
        return empresa;
    }
    
    @Override
    public void setTurno(Shift turno) {
        if (turno==Shift.NOCHE) throw new IllegalArgumentException();
        super.setTurno(turno);
    }
    
    @Override
    
      public String toString(){
        String aux;
        aux = super.toString() + " y cuya empresa es " + empresa ;
        return aux;
    }


    @Override
    public int hashCode() {
        int hash;
        hash = super.hashCode();
        hash = 73 * hash + Objects.hashCode(this.empresa);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (!super.equals(obj)) return false;
        final Reponedor other = (Reponedor) obj;
        return Objects.equals(this.empresa, other.empresa);
    }
    
    
}
