package e2_e3;

import java.util.ConcurrentModificationException;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class EuroCoinCollectionTest {
    EuroCoin e1_sp1, // Juan Carlos I coin
             e1_sp2, // Felipe VI coin
             e2_sp_2002,  // Juan Carlos I coin of 2002
             e2_sp_2005,  // Juan Carlos I coin of 2005
             c50_it, 
             c20_fr,
             c20_it,
             c1_pt;
    EuroCoinCollection collection;
    
    
    @Before
    public void setUp() {
        collection = new EuroCoinCollection();
        
        // THIS COINS MUST BE PROPERLY CREATED IN ORDER TO THE TEST TO WORK CORRECTLY
        e1_sp1 = new EuroCoin(Valor.e1, Pais.ES,  "Juan Carlos 1",2000);
        e1_sp2 = new EuroCoin(Valor.e1, Pais.ES, "Felipe VI", 2002);
        e2_sp_2002  = new EuroCoin(Valor.e2, Pais.ES, "Juan Carlos 1", 2002);
        e2_sp_2005  = new EuroCoin(Valor.e2, Pais.ES, "Juan Carlos 1", 2005);
        c50_it = new EuroCoin(Valor.c50, Pais.IT, "Spaghetti", 2000);
        c20_fr = new EuroCoin(Valor.c20, Pais.FR, "Louis XIV", 2000);
        c20_it = new EuroCoin(Valor.c20, Pais.IT, "Spaghetti", 2000);
        c1_pt = new EuroCoin(Valor.c1, Pais.PT,  "Rousas", 2000);
       
        assertTrue(collection.insertCoin(e1_sp1));
        assertTrue(collection.insertCoin(e1_sp2));
        assertTrue(collection.insertCoin(e2_sp_2002));
        assertFalse(collection.insertCoin(e2_sp_2005)); // Not inserted
        assertTrue(collection.insertCoin(c50_it));
        assertTrue(collection.insertCoin(c20_fr));
        assertFalse(collection.insertCoin(c50_it)); // Not inserted        
    }
    
    @Test
    public void testNumCoins() {       
        assertEquals(5, collection.numCoins());
    }

    @Test
    public void testHasCoin() {
        assertTrue(collection.hasCoin(e1_sp1));
        assertTrue(collection.hasCoin(e2_sp_2005));
        assertFalse(collection.hasCoin(c1_pt));
    }

    @Test
    public void testCollectionValue() {
        assertEquals(470, collection.value());
    }    
    
    @Test
    public void testRemoveCoin() {
        collection.removeCoin(c50_it);
        assertEquals(4, collection.numCoins());
        assertEquals(420, collection.value());
        
        collection.removeCoin(e2_sp_2005); // removes the 2002 coin
        assertEquals(3, collection.numCoins());
        assertEquals(220, collection.value());
        
        collection.removeCoin(c1_pt); // No coin removed
        assertEquals(3, collection.numCoins());
        assertEquals(220, collection.value());
    } 
    
    @Test
    public void testEquals() {
        assertTrue(collection.equals(collection));
        assertFalse(collection.equals(null));
        assertFalse(collection.equals("No es igual"));
    }
    
    @Test
    public void testHashCode() {
        EuroCoinCollection collection2 = new EuroCoinCollection();
        collection2.insertCoin(e1_sp1);
        collection2.insertCoin(e1_sp2);
        collection2.insertCoin(e2_sp_2002);
        collection2.insertCoin(c50_it);
        collection2.insertCoin(c20_fr);
        
        assertTrue(collection.hashCode() == collection.hashCode());
        assertTrue(collection2.hashCode() == collection.hashCode());
    }
    
    @Test
    public void testOrderList (){
        EuroCoinCollection collectionO = new EuroCoinCollection();
        collectionO.insertCoin(e2_sp_2002);
        collectionO.insertCoin(e1_sp2);
        collectionO.insertCoin(e1_sp1);
        collectionO.insertCoin(c50_it);
        collectionO.insertCoin(c20_fr);
        collection.orderList();
        assertEquals(collectionO, collection);
    }
    
    @Test
    public void testOrderListC () {
        EuroCoinCollection collectionO = new EuroCoinCollection();
        Comparador comparador = new Comparador();
        collectionO.insertCoin(c20_fr);
        collectionO.insertCoin(c50_it);
        collectionO.insertCoin(e2_sp_2002);
        collectionO.insertCoin(e1_sp1);
        collectionO.insertCoin(e1_sp2);
        collection.orderListC(comparador);
        assertEquals(collectionO, collection);
    }
    
    @Test(expected=IllegalStateException.class)
    public void testIteradorError() {
        collection.setPaisIterador(Pais.ES);
        Iterator<EuroCoin> iterador = collection.iterator();
        iterador.remove();
    }
    
    @Test(expected=IllegalStateException.class)
    public void testIteradorError2() {
        collection.setPaisIterador(Pais.ES);
        Iterator<EuroCoin> iterador = collection.iterator();
        iterador.next();
        iterador.remove();
        iterador.remove();
    }
    
    @Test
    public void testIterador() {
        collection.setPaisIterador(Pais.IT);
        assertTrue(collection.hasCoin(c50_it));
        Iterator<EuroCoin> iterador = collection.iterator();
        assertTrue(iterador.hasNext());
        assertEquals(c50_it,iterador.next());
        assertFalse(iterador.hasNext());
        iterador.remove();
        assertEquals(4, collection.numCoins());
        assertEquals(420, collection.value());
    }
    
    @Test (expected=NoSuchElementException.class)
    public void testNotFoundNext(){
        collection.setPaisIterador(Pais.EE);
        Iterator<EuroCoin> iterador = collection.iterator();
        iterador.next();
    }
    
    @Test (expected=ConcurrentModificationException.class)
    public void testModification() {
        collection.setPaisIterador(Pais.ES);
        Iterator <EuroCoin> iterador = collection.iterator();
        collection.insertCoin(c1_pt);
        iterador.next();
    }
    
    @Test
    public void testNullIterator() {
        collection.setPaisIterador(null);
        Iterator<EuroCoin> iterador = collection.iterator();
        assertTrue(iterador.hasNext());
        assertEquals(iterador.next(), e1_sp1);
        assertEquals(iterador.next(), e1_sp2);
        assertEquals(iterador.next(), e2_sp_2002);
        assertEquals(iterador.next(), c50_it);
        assertEquals(iterador.next(), c20_fr);
        assertFalse(iterador.hasNext());
    }
    
    @Test
    public void testTwoIterators() {
        collection.insertCoin(c20_it);
        collection.setPaisIterador(Pais.ES);
        Iterator <EuroCoin> iteradorE = collection.iterator();
        iteradorE.next();
        collection.setPaisIterador(Pais.IT);
        Iterator <EuroCoin> iteradorI = collection.iterator();
        iteradorI.next();
        iteradorE.next();
        iteradorE.remove();
        iteradorE.next();
    }
    
    @Test (expected=ConcurrentModificationException.class)
    public void testTwoIteratorsException() {
        collection.insertCoin(c20_it);
        collection.setPaisIterador(Pais.ES);
        Iterator <EuroCoin> iteradorE = collection.iterator();
        iteradorE.next();
        collection.setPaisIterador(Pais.IT);
        Iterator <EuroCoin> iteradorI = collection.iterator();
        iteradorI.next();
        iteradorE.next();
        iteradorE.remove();
        iteradorI.hasNext();
    }
}
