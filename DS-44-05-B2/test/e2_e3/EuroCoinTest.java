/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e2_e3;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class EuroCoinTest {
    EuroCoin e1_sp1;
    
    
    
    @Before
    public void setUp() {
       e1_sp1 = new EuroCoin(Valor.e1, Pais.ES, "Juan Carlos 1",2000);
        
    }
   

    @Test
    public void testGetters() {
        assertEquals(Valor.e1,e1_sp1.getValor());
        assertEquals(Pais.ES, e1_sp1.getPais());
        assertEquals(Color.Oro_Plata, e1_sp1.getColor());
        assertEquals("Juan Carlos 1", e1_sp1.getDiseño());
        assertEquals(2000, e1_sp1.getYear());   
    }
    
    @Test
    public void testHashCode(){
        EuroCoin e1_sp2 = new EuroCoin(Valor.e1, Pais.ES, "Juan Carlos 1",2000);
        assertEquals(e1_sp1.hashCode(),e1_sp2.hashCode());
        assertEquals(e1_sp1.hashCode(),e1_sp1.hashCode());   
    }
    
    @Test
    public void testCompareTo(){
        EuroCoin e1_sp2 =new EuroCoin(Valor.e1, Pais.ES, "Juan Carlos 1",2000);
        EuroCoin e2_sp1 =new EuroCoin(Valor.e2, Pais.ES,  "Juan Carlos 1",2000);
        EuroCoin c50_sp1 =new EuroCoin(Valor.c50, Pais.ES,  "Juan Carlos 1",2000);
        EuroCoin e1_at1 =new EuroCoin(Valor.e1, Pais.AT,  "Juan Carlos 1",2000);
        EuroCoin e1_va1 =new EuroCoin(Valor.e1, Pais.VA,  "Juan Carlos 1",2000);
        EuroCoin e1_sp3 =new EuroCoin(Valor.e1, Pais.ES,  "Felipe VI",2000);
        EuroCoin e1_sp4 =new EuroCoin(Valor.e1, Pais.ES,  "Rey Juan Carlos 1",2000);
        
        assertEquals(0, e1_sp1.compareTo(e1_sp2));
        assertTrue(e1_sp1.compareTo(e2_sp1)>0);
        assertTrue(e1_sp1.compareTo(c50_sp1)<0);
        assertTrue(e1_sp1.compareTo(e1_at1)>0);
        assertTrue(e1_sp1.compareTo(e1_va1)<0);
        assertTrue(e1_sp1.compareTo(e1_sp3)>0);
        assertTrue(e1_sp1.compareTo(e1_sp4)<0);        
    
    
    
    }
}
