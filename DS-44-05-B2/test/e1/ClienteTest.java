
package e1;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


public class ClienteTest {
    Cliente Andrea;
    Cliente Jose;
    Cliente Sara;
    Cliente Alex;
    
    public ClienteTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        Andrea= new Cliente("Andrea","Bocas","998871221R","Cambados",665456576,13445,300);
        Jose= new Cliente("Jose","Piernas","932111221E","Cambados",15655,200);
        Sara= new Cliente("Sara","Cabeza","988321121W",443768799,11233,700);
        Alex= new Cliente("Alex","Costado","321187122T",65443,900);
        
        
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetters(){
       assertEquals("Andrea",Andrea.getNombre());
       assertEquals("Bocas",Andrea.getApellidos());
       assertEquals("998871221R",Andrea.getDNI());
       assertEquals("Cambados",Andrea.getDireccion());
       assertEquals(665456576,Andrea.getTelefono());
       assertEquals(13445,Andrea.getCodigo());
       assertEquals(300,Andrea.getCompras());
       assertEquals(3,Andrea.getPorcentajedescuento());
    }
       
    @Test
    public void testSetters() {
        Andrea.setCompras(100);
        Andrea.setDireccion("A Coruña");
        Andrea.setTelefono(125478954);
        assertEquals(Andrea.getCompras(), 100);
        assertEquals(Andrea.getPorcentajedescuento(), 1);
        assertEquals(Andrea.getDireccion(), "A Coruña");
        assertEquals(Andrea.getTelefono(),125478954 );
    }
    


    @Test
    public void testMasCompras() {
        Andrea.masCompras();
       assertEquals(301,Andrea.getCompras());
    }


    @Test
    public void testToString() {
       assertEquals("Andrea Bocas, con DNI 998871221R, dirección Cambados, teléfono 665456576, codigo de cliente 13445, numero de compras 300 y porcentaje de descuento 3",Andrea.toString());
    }

    @Test
    public void testHashCode() {
       Cliente Andrea2 = new Cliente("Andrea","Bocas","998871221R","Cambados",665456576,13445,300);
       assertTrue(Andrea.hashCode() == Andrea2.hashCode());
       assertTrue(Andrea.hashCode() == Andrea.hashCode());
       
    }


    @Test
    public void testEquals() {
     Cliente Andrea2 = new Cliente("Andrea","Bocas","998871221R","Cambados",665456576,13445,300);
     Cliente NotAndrea = new Cliente("NotAndrea","Bocas","998871221R","Cambados",665456576,13445,300);
     assertTrue(Andrea.equals(Andrea));
     assertFalse(Andrea.equals(null));
     assertFalse(Andrea.equals("No es igual"));
     assertFalse(Andrea.equals(NotAndrea));
     assertTrue(Andrea.equals(Andrea2));
    }
    
}
