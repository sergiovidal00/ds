package e1;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class DependienteTest {
   Dependiente Pedro;
   
    @Before
    public void setUp() {
        Pedro= new Dependiente("Pedro","López López","998877661A","Brión",669836788,123432456,700,Trabajador.Shift.TARDE,"Transporte");   
    }
    
    @Test
    public void testGetters (){
        assertEquals("Pedro", Pedro.getNombre());
        assertEquals("López López", Pedro.getApellidos());
        assertEquals("998877661A",Pedro.getDNI());
        assertEquals("Brión", Pedro.getDireccion());
        assertEquals(669836788,Pedro.getTelefono());
        assertEquals(123432456, Pedro.getNumeroSS());
        assertEquals(700, Pedro.getSalario());
        assertEquals(Trabajador.Shift.TARDE, Pedro.getTurno());
        assertEquals("Transporte", Pedro.getEspecialidad());   
    }
    
    @Test
    public void testToString(){
        assertEquals("Pedro López López, con DNI 998877661A, dirección Brión, teléfono 669836788, cuyo numero de la seguridad social es 123432456 y salario 700 y su turno es de TARDE y cuya especialidad es Transporte", Pedro.toString());
    }
    
    @Test
    public void testEquals() {
        Dependiente Peter = new Dependiente("Pedro","López López","998877661A","Brión",669836788,123432456,700,Trabajador.Shift.TARDE,"Transporte");   
        Dependiente NotPeter = new Dependiente("No Pedro","López López","998877661A","Brión",669836788,123432456,700,Trabajador.Shift.TARDE,"Transporte");   
        assertTrue(Pedro.equals(Pedro));
        assertFalse(Pedro.equals(null));
        assertFalse(Pedro.equals("No es igual"));
        assertFalse(Pedro.equals(NotPeter));
        assertTrue(Pedro.equals(Peter));
        
    }
    
    @Test
    public void testHashcode(){
        Dependiente Peter = new Dependiente("Pedro","López López","998877661A","Brión",669836788,123432456,700,Trabajador.Shift.TARDE,"Transporte");   
        assertTrue(Pedro.hashCode() == Pedro.hashCode());
        assertTrue(Pedro.hashCode() == Peter.hashCode());
        
    }
}
