package e1;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class ReponedorTest {
   Reponedor Juan;
   
    @Before
    public void setUp() {
        Juan = new Reponedor("Juan", "Pérez Pérez", "12345678E","A Coruña", 123456789, 987654321, 500, Trabajador.Shift.MAÑANA, "Copinsa");   
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testNightShift (){
        Reponedor Pepe =new Reponedor("Pepe", "Pérez Pérez", "12345678E","A Coruña", 123456789, 987654321, 500, Trabajador.Shift.NOCHE, "Copinsa");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testSetNightShift (){
        Juan.setTurno(Trabajador.Shift.NOCHE);
    }
    
    @Test
    public void testGetters (){
        assertEquals("Juan", Juan.getNombre());
        assertEquals("Pérez Pérez", Juan.getApellidos());
        assertEquals("12345678E",Juan.getDNI());
        assertEquals("A Coruña", Juan.getDireccion());
        assertEquals(123456789,Juan.getTelefono());
        assertEquals(987654321, Juan.getNumeroSS());
        assertEquals(500, Juan.getSalario());
        assertEquals(Trabajador.Shift.MAÑANA, Juan.getTurno());
        assertEquals("Copinsa", Juan.getEmpresa());   
    }
    
    @Test
    public void testSetters(){
        Juan.setSalario(1000);
        Juan.setTurno(Trabajador.Shift.TARDE);
        assertEquals(1000, Juan.getSalario());
        assertEquals(Trabajador.Shift.TARDE, Juan.getTurno());
    }
    
    
    @Test
    public void testToString(){
        assertEquals("Juan Pérez Pérez, con DNI 12345678E, dirección A Coruña, teléfono 123456789, cuyo numero de la seguridad social es 987654321 y salario 500 y su turno es de MAÑANA y cuya empresa es Copinsa", Juan.toString());
    }
    
    @Test
    public void testEquals() {
        Reponedor John = new Reponedor("Juan", "Pérez Pérez", "12345678E","A Coruña", 123456789, 987654321, 500, Trabajador.Shift.MAÑANA, "Copinsa");   
        Reponedor NotJohn = new Reponedor("Not Juan", "Pérez Pérez", "12345678E","A Coruña", 123456789, 987654321, 500, Trabajador.Shift.MAÑANA, "Copinsa");   
        assertTrue(Juan.equals(Juan));
        assertFalse(Juan.equals(null));
        assertFalse(Juan.equals("No es igual"));
        assertFalse(Juan.equals(NotJohn));
        assertTrue(Juan.equals(John));
        John.setTurno(Trabajador.Shift.TARDE);
        assertFalse(Juan.equals(John));
        John.setTurno(Trabajador.Shift.MAÑANA);
        John.setSalario(1000);
        assertFalse(Juan.equals(John));
    }
    
    @Test
    public void testHashcode(){
        Reponedor John= new Reponedor("Juan", "Pérez Pérez", "12345678E","A Coruña", 123456789, 987654321, 500, Trabajador.Shift.MAÑANA, "Copinsa");   
        assertTrue(Juan.hashCode() == Juan.hashCode());
        assertTrue(Juan.hashCode() == John.hashCode());
        
    }
}
