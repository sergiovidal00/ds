package e1;

import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.*;

public class MercadoTest {
    Reponedor Juan, Jose, Ramon, Jonas;
    Dependiente Pedro, Isidro, Fran, Sergio;
    Cliente Tamara, David, Alex, Pablo;
    
    Mercado mercado;
    
    @Before
    public void setUp() {
        mercado = new Mercado();
        List <Cliente> clientes = new ArrayList<>();
        List <Reponedor> reponedores = new ArrayList<>();
        List <Dependiente> dependientes = new ArrayList<>();
        
        Juan = new Reponedor("Juan", "Pérez Pérez", "12345678E","A Coruña", 123456789, 987654321, 500, Trabajador.Shift.MAÑANA, "Copinsa");
        Jose = new Reponedor ("Jose", "Lopez Lopez", "21345698R", "Lugo", 985645216, 500, Trabajador.Shift.MAÑANA, "El Corte Ingles");
        Ramon = new Reponedor ("Ramon", "Garcia Garcia", "58489484E", 8484549, 584849484, 220,Trabajador.Shift.TARDE, "Carrefour");
        Jonas = new Reponedor ("Jonas", "Apellido Apellido", "54895156F", 547789562, 700, Trabajador.Shift.TARDE, "Oracle");
        
        Pedro= new Dependiente("Pedro","López López","998877661A","Brión",669836788,123432456,700,Trabajador.Shift.TARDE,"Transporte");
        Isidro= new Dependiente("Isidro","Techado Liso","454333121R","Alepo",9349432,7500,Trabajador.Shift.MAÑANA,"Ejecutivo");
        Fran= new Dependiente("Fran","Ali Osman","111998456R",123321123,555555999,300,Trabajador.Shift.NOCHE,"Limpieza");
        Sergio= new Dependiente("Sergio","Ñoñeño Ñiñoñe","008854231P",493586736,4050,Trabajador.Shift.MAÑANA,"Seguridad");
        
        Tamara = new Cliente ("Tamara", "Falso Vigo", "87489595Q", "Madrid", 987546123, 98845, 500);
        David = new Cliente ("David", "Falso Vigo", "98452146T", "Barcelona", 54862, 200);
        Alex = new Cliente ("Alex", "Gonzalez Gonzalez", "84577855P", 894567512, 85484, 100);
        Pablo = new Cliente ("Pablo", "Perez Garcia", "87459621R", 12347, 600);
        
        clientes.add(Tamara);
        clientes.add(David);
        assertTrue(mercado.agregarCliente(Alex));
        assertFalse(mercado.agregarCliente(Alex));
        assertTrue(mercado.AgregarClientes(clientes));
        assertFalse(mercado.AgregarClientes(clientes));
        reponedores.add(Ramon);
        reponedores.add(Jose);
        assertTrue(mercado.agregarTrabajador(Juan));
        assertFalse(mercado.agregarTrabajador(Juan));
        assertTrue(mercado.agregarTrabajador(Jose));
        assertFalse(mercado.agregarTrabajadores(reponedores));
        dependientes.add(Pedro);
        dependientes.add(Isidro);
        assertTrue(mercado.agregarTrabajador(Fran));
        assertTrue(mercado.agregarTrabajador(Sergio));
        assertTrue(mercado.agregarTrabajadores(dependientes));
        
    }
    
 @Test
 public void TestSalariosMercado (){
    assertEquals(13700, mercado.SalariosMercado());
}
 
 @Test
 public void testPersonasMercado () {
     assertEquals(9,mercado.PersonasMercado());
 }
  
}
