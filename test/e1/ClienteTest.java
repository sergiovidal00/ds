
package e1;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


public class ClienteTest {
    Cliente Andrea;
    Cliente Jose;
    Cliente Sara;
    Cliente Alex;
    
    public ClienteTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        Andrea= new Cliente("Andrea","Bocas","998871221R","Cambados",665456576,13445,300);
        Jose= new Cliente("Jose","Piernas","932111221E","Cambados",15655,200);
        Sara= new Cliente("Sara","Cabeza","988321121W",443768799,11233,700);
        Alex= new Cliente("Alex","Costado","321187122T",65443,900);
        
        
    }
    
    @After
    public void tearDown() {
    }

  
    @Test
    public void testGetCodigo() {
       assertEquals(13445,Andrea.getCodigo());
       assertEquals(15655,Jose.getCodigo());
       assertEquals(11233,Sara.getCodigo());
       assertEquals(65443,Alex.getCodigo());  
    }

    @Test
    public void testGetCompras() {
       assertEquals(300,Andrea.getCompras());
       assertEquals(200,Jose.getCompras());
       assertEquals(700,Sara.getCompras());
       assertEquals(900,Alex.getCompras()); 
    }

    @Test
    public void testGetPorcentajedescuento() {
       assertEquals(3,Andrea.getPorcentajedescuento());
       assertEquals(2,Jose.getPorcentajedescuento());
       assertEquals(7,Sara.getPorcentajedescuento());
       assertEquals(9,Alex.getPorcentajedescuento());
    }


    @Test
    public void testMasCompras() {
       assertEquals(303,Andrea.getPorcentajedescuento()+Andrea.getCompras());
       assertEquals(202,Jose.getPorcentajedescuento()+Jose.getCompras());
       assertEquals(707,Sara.getPorcentajedescuento()+Sara.getCompras());
       assertEquals(909,Alex.getPorcentajedescuento()+Alex.getCompras());
    }


    @Test
    public void testToString() {
       assertEquals("Andrea Bocas 998871221R Cambados 665456576 13445 300",Andrea.toString());
       assertEquals("Jose Piernas 932111221E Cambados 15655 200",Jose.toString());
       assertEquals("Sara Cabeza 988321121W 443768799 11233 700",Sara.toString());
       assertEquals("Alex Costado 321187122T 65443 900",Alex.toString());
    }

    @Test
    public void testHashCode() {
       Cliente Andrea2 = new Cliente("Andrea","Bocas","998871221R","Cambados",665456576,13445,300);
       assertTrue(Andrea.hashCode() == Andrea2.hashCode());
       assertTrue(Andrea.hashCode() == Andrea.hashCode());
       
    }


    @Test
    public void testEquals() {
     Cliente Andrea2 = new Cliente("Andrea","Bocas","998871221R","Cambados",665456576,13445,300);
     Cliente NotAndrea = new Cliente("NotAndrea","Bocas","998871221R","Cambados",665456576,13445,300);
     assertTrue(Andrea.equals(Andrea));
     assertFalse(Andrea.equals(null));
     assertFalse(Andrea.equals("No es igual"));
     assertFalse(Andrea.equals(NotAndrea));
     assertTrue(Andrea.equals(Andrea2));
    }
    
}
