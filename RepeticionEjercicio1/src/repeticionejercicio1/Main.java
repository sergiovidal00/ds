package repeticionejercicio1;

public class Main {

    public static void main(String[] args) {
        Carta carta = new Carta(7, "Bastos");
        
        System.out.println(carta.getNumero()+ " de " + carta.getPalo());
        
        Baraja baraja = new Baraja();
        
        
        for (int i=0; i<40; i++){
          System.out.println(baraja.carta[i].getNumero() + " de " + baraja.carta[i].getPalo());
        }
        
    }
    
}
