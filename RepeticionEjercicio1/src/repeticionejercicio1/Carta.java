package repeticionejercicio1;

/**
 *
 * @author Sergio
 */
public class Carta {
    private String palo;
    private int numero;

    public String getPalo() {
        return palo;
    }

    public void setPalo(String palo) {
        this.palo = palo;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }
    
    public Carta (int n, String p) {
        numero=n;
        palo=p;
        
        }
    
    
    
    
}
