package ejercicio1;

public class Carta {
    private int numero;
    
    private String palo;

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getPalo() {
        return palo;
    }

    public void setPalo(String palo) {
        this.palo = palo;
    }
    
    public Carta(int n, String p) {
        numero= n;
        palo= p;
    }
    
}
